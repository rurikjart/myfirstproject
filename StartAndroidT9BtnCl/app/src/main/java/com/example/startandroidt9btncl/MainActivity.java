package com.example.startandroidt9btncl;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    TextView textView;
    Button button1;
    Button button2;
    Button button3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //определяем компоненты на интерфейсе
        textView = (TextView) findViewById(R.id.textView);

        button1 = (Button) findViewById(R.id.button);
        button2 = (Button) findViewById(R.id.button2);
        button3 = (Button) findViewById(R.id.button3);



        //первый метод N 1 обработчика на кнопку. назначаем стандартный слушатель. Через ананимный метод
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textView.setText("Нажата кнопка 1");
            }
        });

        button2.setOnClickListener(this);
    }

    // Второй метод N2 через интерфейс который снабжает навыками текущее активити
    @Override
    public void onClick(View v) {
        textView.setText("Нажата кнопка 2");
    }

   // Третий способ N 3 прописываем название обработчика в файле макета
    public void onClickBtn(View v){
        textView.setText("Нажата кнопка 3");
    }



}




