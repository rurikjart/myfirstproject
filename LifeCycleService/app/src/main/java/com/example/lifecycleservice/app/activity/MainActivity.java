package com.example.lifecycleservice.app.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.lifecycleservice.app.R;
import com.example.lifecycleservice.app.service.MyLifeCycleService;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       //startService(new Intent(this, MyService.class));
       setContentView(R.layout.activity_main);
    }

    public void startService(View view){
        startService(new Intent(this, MyLifeCycleService.class));
    }

    public void stopService(View view){
        stopService(new Intent(this, MyLifeCycleService.class));
    }

}
