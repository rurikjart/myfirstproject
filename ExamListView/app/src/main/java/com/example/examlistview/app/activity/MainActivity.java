package com.example.examlistview.app.activity;


import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.examlistview.app.R;
import com.example.examlistview.app.adapter.PhoneModelAdapter;
import com.example.examlistview.app.pojo.PhoneModel;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private  ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // находим на интерфейсе список
        listView = (ListView) findViewById(R.id.listView);

        // для удобства выгружаем в список
       // List<String> items = initData();

        // адаптируем информацию для отображения в визуальном компоненте listView
       // ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,items);
        PhoneModelAdapter adapter = new PhoneModelAdapter(this, initData());
        // Присваиваем адаптер системе отображения
        listView.setAdapter(adapter);
    }

    private List<PhoneModel> initData(){
        List<PhoneModel> list = new ArrayList<PhoneModel>();

        list.add(new PhoneModel(1,"IPhone", 1000));
        list.add(new PhoneModel(2,"HTC", 4546));
        list.add(new PhoneModel(3,"Samsung", 888));
        list.add(new PhoneModel(4,"LG", 700));

        return  list;
    }

}

