package com.example.myexamplemenu.app;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import com.example.myexamplemenu.R;




public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_items, menu);

        return true;
    }

    public void onAbout(MenuItem item){
        Toast.makeText(this,"Об авторе", Toast.LENGTH_SHORT).show();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item){

       switch (item.getItemId()){

           case R.id.settings:
               Toast.makeText(this,"Вы выбрали настройки", Toast.LENGTH_SHORT).show();
               return true;

         //  case R.id.about:
          //     Toast.makeText(this,"Вы выбрали Об авторе", Toast.LENGTH_SHORT).show();
        //       return true;

           case R.id.site:
               Toast.makeText(this,"Вы выбрали Off. Сайт", Toast.LENGTH_SHORT).show();
               return true;

       }
       return super.onOptionsItemSelected(item);
    }


}
