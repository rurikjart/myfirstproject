package com.example.asynctaskexamt.examasynctask.myapplication2;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.util.concurrent.TimeUnit;

public class MyAsyncTask extends AsyncTask<Void, Void, Void> {

    private static  MyAsyncTask instance;

    private static Context context;



    public static MyAsyncTask  getInstance(Context context) {
        if (instance == null){
            instance = new MyAsyncTask(context);
            instance.execute();
        }
        setContext(context);

        return  instance;
    }

    public MyAsyncTask(Context context) {
        this.context = context;
    }

    @Override
    protected Void doInBackground(Void... voids) {

        try{
            for (int i = 0; 1<=5; i++){
               Log.i("MY TASK", "Task is working ...");

                TimeUnit.SECONDS.sleep(1);
                printInfo();
                if (isCancelled()){
                    return  null;
                }
            }

        }
        catch (InterruptedException e){
                e.printStackTrace();
        }


        return null;
    }

    private void printInfo() {
        Log.i("DEV MODE", "CODE: " + this.hashCode() + "|" + context.hashCode());
    }

    public static void setContext(Context context) {
        MyAsyncTask.context = context;
    }

    protected void onPostExecute(Void aVoid){
        instance = null;
    }
}
