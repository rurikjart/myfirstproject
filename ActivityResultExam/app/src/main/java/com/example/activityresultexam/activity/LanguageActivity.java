package com.example.activityresultexam.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.activityresultexam.R;
import com.example.activityresultexam.util.Language;
import com.example.activityresultexam.util.RequestCode;

public class LanguageActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.language_layout);
    }

    public void onSelectedLanguage(View view){
        Intent intent;
        switch (view.getId()){
            case R.id.btnEnglish:
                intent = new Intent();
                intent.putExtra("language", Language.ENGLISH.getLanguage());
                setResult(RESULT_OK, intent);
                finish();
                break;
            case R.id.btnRussian:
                intent = new Intent();
                intent.putExtra("language", Language.RUSSIAN.getLanguage());
                setResult(RESULT_OK, intent);
                finish();
                break;
            case R.id.btnUkrainian:
                intent = new Intent();
                intent.putExtra("language", Language.UKRAINE.getLanguage());
                setResult(RESULT_OK, intent);
                finish();
                break;
        }
    }

}
