package com.example.activityresultexam.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.view.View;

import com.example.activityresultexam.R;

import java.sql.ResultSet;


public class PresentedActivity extends Activity {

    private EditText editName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.presented_layout);

        editName = (EditText)findViewById(R.id.editName);

    }

    public void onPresented(View v){
        Intent intent = new Intent();
        intent.putExtra("name",editName.getText().toString());
        setResult(RESULT_OK, intent);
        finish();
    }
}
