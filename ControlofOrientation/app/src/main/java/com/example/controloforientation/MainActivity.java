package com.example.controloforientation;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.widget.Toast;
import android.view.View;


public class MainActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);
    }


    public void onPosition(View v){
        Context applicationContext = getApplicationContext();
        Configuration configuration = getResources().getConfiguration();

        if (configuration.orientation==Configuration.ORIENTATION_PORTRAIT){
            Toast.makeText(applicationContext,"Это портретный вид",Toast.LENGTH_SHORT).show();
        }

        if (configuration.orientation==Configuration.ORIENTATION_LANDSCAPE){
            Toast.makeText(applicationContext,"Это альбомный вид",Toast.LENGTH_SHORT).show();
        }
    }
}
