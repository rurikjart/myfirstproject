package com.example.alertdialogexam;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;


public class MainActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);
    }


    public void showDialod(View view){

        //Сначала отдельно организыем диалог для дальнейшего показа его пользователю
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

        //заполняем билдер
        builder.setTitle("dev.com")
                .setMessage("Это информационное сообщение")
                .setCancelable(true) // разрешаем использование кнопки назад пользовательского интерфейса Android
                .setIcon(R.mipmap.ic_launcher_round)
                .setPositiveButton("Да", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(getApplicationContext(),"Вы согласны с нами!", Toast.LENGTH_SHORT).show();
                    }
                })

                .setNegativeButton("Нет", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(getApplicationContext(),"Вы не согласны с нами!", Toast.LENGTH_SHORT).show();
                    }
                })

                .setOnCancelListener(new DialogInterface.OnCancelListener(){
                    public  void onCancel(DialogInterface dialog){
                        Toast.makeText(getApplicationContext(),"Вы проигнорировали нас :(", Toast.LENGTH_SHORT).show();
                    }
                });

        AlertDialog alert = builder.create();
        alert.show();

    }
}
