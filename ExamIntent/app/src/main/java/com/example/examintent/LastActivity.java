package com.example.examintent;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


public class LastActivity extends Activity implements View.OnClickListener {

    private Button button2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.last_layout);

        //находим в интерфейсе кнопку возврата
        button2 = (Button) findViewById(R.id.button2);

        //используем тут анониманый метод для обработки нажатия кнопки. через setOnClickListener
        button2.setOnClickListener(this);
       /* button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
            }
        });*/

    }



    //  public void  onBack(View v){
  //      Intent intent = new Intent(this, MainActivity.class);
  //      startActivity(intent);
  //  }

    public void onClick(View v) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
