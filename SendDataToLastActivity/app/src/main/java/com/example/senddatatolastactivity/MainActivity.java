package com.example.senddatatolastactivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;



public class MainActivity extends Activity {

    private EditText sLogin, sPassword;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);

        sLogin = (EditText) findViewById(R.id.edLogin);
        sPassword = (EditText) findViewById(R.id.edPassword);
    }


    public void sendData(View view){
        Intent intent = new Intent(this,LastActivity.class);
        intent.putExtra("sLogin",sLogin.getText().toString());
        intent.putExtra("sPassword",sPassword.getText().toString());

        startActivity(intent);

    }
}
