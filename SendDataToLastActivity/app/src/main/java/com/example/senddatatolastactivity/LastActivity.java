package com.example.senddatatolastactivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.Nullable;

public class LastActivity extends Activity {

    private EditText pLogin, pPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.last_layout);

        pLogin = (EditText) findViewById(R.id.pSendLoginEditText);
        pPassword = (EditText) findViewById(R.id.pSendPasswordEditText);

        pLogin.setText(getIntent().getStringExtra("sLogin"));
        pPassword.setText(getIntent().getStringExtra("sPassword"));
    }


    public void clickBack(View view){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
