package com.example.myfirstproject;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView textView;
    //private Button button2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = (TextView) findViewById(R.id.textView);

        //вытаскиваем кнопку для обработчика из файла макета
        //button2 = (Button) findViewById(R.id.button2);


    }


    public  void  sayHello(View view){
        textView.setText("Выполнен обработчик из объектного инспектора!");
    }

    public  void  sayHelloMaket(View view){
        textView.setText("Выполнен обработчик из файла макета!");
    }
}
