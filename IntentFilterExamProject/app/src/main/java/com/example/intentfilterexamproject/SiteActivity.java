package com.example.intentfilterexamproject;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class SiteActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_site);
    }
}
