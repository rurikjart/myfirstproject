package com.example.androidlogexam;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends Activity {

    private  final String TAG = "DEVR";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);
    }


    public  void onInfoLog(View v){
        Log.i(TAG, "onInfoLog: Info level");
    }

    public  void onErrorLog(View v){
        Log.e(TAG, "onErroLog: Error level" );
    }

    public  void onWarnLog(View v){
        Log.w(TAG, "onWarnLog: Warn level");

    }

    public  void onDebugLog(View v){
      Log.d(TAG, "onDebugLog: Debug level");
    }


    public  void onVerboseLog(View v){
        Log.v(TAG, "onVerboseLog: Verbose level");
    }

    public  void onWTFLog(View v){
        Log.wtf(TAG, "onWTFLog: WTF level");
    }



}


