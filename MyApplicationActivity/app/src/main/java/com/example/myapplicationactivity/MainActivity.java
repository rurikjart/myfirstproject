package com.example.myapplicationactivity;

import androidx.appcompat.app.AppCompatActivity;
import android.app.Activity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    // 1. Вызывается в момент создания активити в самый первый раз после старта приложения
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    // 2. Вызывается после после onCreate или после восстановления активити после переключения задач перед показом пользователю
    protected void onStart() {
        super.onStart();
    }

    @Override
    // 3. Вызывается после onStart когда активити уже показалось пользователю
    protected void onResume() {
        super.onResume();
    }

    @Override
    // 4. Вызывается когда пользователь перешол на другое активити. Событие срабатывает для старого активити. Далее можно вернуться к событию onStop
    protected void onPause() {
        super.onPause();
    }

    @Override
    // 5. Помещает активити в стек.
    protected void onStop() {
        super.onStop();
    }


    @Override
    // 6. Пользователю вернется активити не пересоздавая его
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    // 7. Он срабатывает после события onStop. Либо при выходе из приложения. Либо при удалении активити при работе приложения при отчиствки кучи.
    protected void onDestroy() {
        super.onDestroy();
    }

}

